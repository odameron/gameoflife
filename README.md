# gameOfLife

A simple implementation of John Conway's famous game of life.

# Todo

- [ ] add code skeletons for generating images
    - [ ] java
    - [ ] python
- [ ] add a functionality for generating a matrix of the counts of evolutionary steps during which each cell was live
- [ ] add a functionality for taking a QRcode as the initial grid
- [ ] add a function for recognizing patterns such as gliders (including rotated)
- [ ] add a function for counting the number of live cells in a grid
    - [ ] initialize a grid with different rates of live cells, and plot the proportion of live cells after one step as a function of the initial rate of live cells
- [ ] add a function that prints the neighbors of a cell of interest
- [ ] modify the function that computes the next grid so that it returns a boolean indicating whether there is at least one live cell in the grid (in order to prevent having to go through the grid for determining whether to exit the loop of evolutionary steps)
